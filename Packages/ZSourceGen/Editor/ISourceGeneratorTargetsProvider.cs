﻿using System;
using System.Collections.Generic;
using ZSourceGenerator;

public interface ISourceGeneratorTargetsProvider
{
    /// <summary>
    /// Returns true if this provider provides targets for the generator in question.
    /// </summary>
    bool IsProviderForGenerator(ISourceGenerator generator);
    
    IEnumerable<Type> ClassesRegisteredForGenerator();
}