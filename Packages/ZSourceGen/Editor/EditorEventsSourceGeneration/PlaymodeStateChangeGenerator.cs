﻿using Settings;
using UnityEditor;

namespace EditorEventsSourceGeneration
{
    [InitializeOnLoad]
    public class PlaymodeStateChangeGenerator
    {
        static PlaymodeStateChangeGenerator()
        {
            EditorApplication.playModeStateChanged += OnPlaymodeStateChanged;
        }

        ~PlaymodeStateChangeGenerator()
        {
            EditorApplication.playModeStateChanged -= OnPlaymodeStateChanged;
        }
        
        private static void OnPlaymodeStateChanged(PlayModeStateChange change)
        {
            var settings = GeneratorSettingsEditorPrefsPersistence.LoadSettings();
            if (!settings.RunAllGeneratorsBeforeEnteringPlayMode)
            {
                return;
            }
            
            if (change == PlayModeStateChange.ExitingEditMode)
            {
                DirectGenerationCommands.RunAllGeneratorsAndLogGenerationTime();
            }
        }
    }
}