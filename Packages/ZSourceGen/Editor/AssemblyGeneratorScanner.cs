﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using ZSourceGenerator;

public class AssemblyGeneratorScanner : IGeneratorScanner
{
    public IEnumerable<ISourceGenerator> ScanGenerators(Assembly[] assemblies)
    {
        List<ISourceGenerator> generators = new List<ISourceGenerator>();

        foreach (Type type in GetTypesImplementingInterface(assemblies, typeof(ISourceGenerator)))
        {
            if (type.IsAbstract)
            {
                continue;
            }

            if (IsMonoBehaviorType(type))
            {
                Debug.LogError($"Type {type.Name} inherits from {nameof(MonoBehaviour)}" +
                               $" and this is not supported. Skipped.");
                continue;
            }
            
            if (IsScriptableObjectType(type))
            {
                LoadAndRegisterScriptableObjectGenerators(type, generators);
                continue;
            }

            if (Activator.CreateInstance(type) is ISourceGenerator sourceGenerator)
            {
                generators.Add(sourceGenerator);
            }
        }

        return generators;
    }

    private static void LoadAndRegisterScriptableObjectGenerators(Type type, ICollection<ISourceGenerator> generators)
    {
        var scriptableObjects = Resources.FindObjectsOfTypeAll(type);
        foreach (var obj in scriptableObjects)
        {
            if (obj is ISourceGenerator scriptableSourceGenerator)
            {
                generators.Add(scriptableSourceGenerator);
            }
        }
    }

    private static bool IsMonoBehaviorType(Type type)
    {
        return typeof(MonoBehaviour).IsAssignableFrom(type);
    }
    
    private static bool IsScriptableObjectType(Type type)
    {
        return typeof(ScriptableObject).IsAssignableFrom(type);
    }

    private static IEnumerable<Type> GetTypesImplementingInterface(Assembly[] assemblies, Type desiredType)
    {
        foreach (Assembly assembly in assemblies)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (desiredType.IsAssignableFrom(type) && !type.IsInterface)
                {
                    yield return type;
                }
            }
        }
    }
}