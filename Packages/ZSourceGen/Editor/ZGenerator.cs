﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using Commands;
using Settings;
using Util;
using ZSourceGenerator;

public class ZGenerator
{
    private string DefaultDirectory() => 
        string.IsNullOrEmpty(_settings?.DefaultGeneratorOutputDirectory) ? 
        "Assets/GeneratedSource" : 
        _settings.DefaultGeneratorOutputDirectory;
    
    private readonly IGeneratorScanner _generatorScanner;
    private readonly GeneratorSettings _settings;
    
    private readonly List<ISourceGeneratorTargetsProvider> _targetProviders = new();
    private readonly HashSet<IGenerationPostProcessor> _generationPostProcessors = new();
    
    public ZGenerator(IGeneratorScanner generatorScanner, GeneratorSettings settings)
    {
        _generatorScanner = generatorScanner;
        _settings = settings;
    }
    
    public void RegisterTargetProvider(ISourceGeneratorTargetsProvider provider)
    {
        _targetProviders.Add(provider);
    }

    /// <summary>
    /// Postprocessors execute after source code has been generated but before source files are created.
    /// </summary>
    /// <param name="processor"></param>
    public void RegisterGenerationPostProcessor(IGenerationPostProcessor processor)
    {
        _generationPostProcessors.Add(processor);
    }

    public void GenerateAllSources()
    {
        Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
        IEnumerable<ISourceGenerator> generators = _generatorScanner.ScanGenerators(assemblies);

        List<GenerationCommand> generationCommands = new();
        foreach (var generator in generators)
        {
            generationCommands.AddRange(CreateGeneratorCommandsForGenerator(generator));
        }

        RunGenerationPostProcessors(generationCommands);
        ExecuteGenerationCommands(generationCommands);
    }
    
    public void RunSingleGenerator(ISourceGenerator generator)
    {
        var commands = CreateGeneratorCommandsForGenerator(generator);
        
        RunGenerationPostProcessors(commands);
        ExecuteGenerationCommands(commands);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private void RunGenerationPostProcessors(List<GenerationCommand> commands)
    {
        // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
        foreach (var processor in _generationPostProcessors)
        {
            processor.Process(commands);
        }
    }
    
    private GenerationCommand CreateSingleGenerationCommand(ISourceGenerator sourceGenerator, GenerationContext context)
    {
        string generatedSource = sourceGenerator.GenerateSource(context);

        generatedSource = AddOptionalGeneratedHeader(generatedSource);
        
        string directory = string.IsNullOrEmpty(sourceGenerator.SourceDirectory) 
            ? DefaultDirectory()
            : $"Assets/{sourceGenerator.SourceDirectory}";
        
        string generatedFileName = string.IsNullOrEmpty(sourceGenerator.GeneratedFileName) ? 
            sourceGenerator.GetType().Name :
            sourceGenerator.GeneratedFileName;
        
        string filePath = Path.Combine(directory, $"{generatedFileName}{_settings.GeneratedExtension}");
        
        return new GenerationCommand(directory, filePath, generatedSource);
    }

    private string AddOptionalGeneratedHeader(string generatedSource)
    {
        if (string.IsNullOrEmpty(_settings.GeneratedFilesHeader)) 
            return generatedSource;
        return _settings.GeneratedFilesHeader + generatedSource;
    }

    private List<GenerationCommand> CreateGeneratorCommandsForGenerator(ISourceGenerator generator)
    {
        List<GenerationCommand> commands = new();
        
        var context = new GenerationContext();

        ISourceGeneratorTargetsProvider provider = GetTargetProviderForGenerator(generator);
        if (provider is not null)
        {
            commands.AddRange(CreateGenerationCommandsForTargets(generator, provider, context));
        }
        else
        {
            if (generator.ShouldGenerate(context))
            {
                commands.Add(CreateSingleGenerationCommand(generator, context));
            }
        }

        return commands;
    }

    private void ExecuteGenerationCommands(IEnumerable<GenerationCommand> commands)
    {
        foreach (var command in commands)
        {
            ExecuteGenerationCommand(command);
        }
    } 
    
    private void ExecuteGenerationCommand(GenerationCommand command)
    {
        DirectoryUtil.EnsureDirectoryExists(command.Directory);
        _settings.FileWriter.WriteSourceFile(command.FilePath, command.SourceCode);
    }
    
    private ISourceGeneratorTargetsProvider GetTargetProviderForGenerator(ISourceGenerator generator)
    {
        ISourceGeneratorTargetsProvider provider = null;
        // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
        foreach (ISourceGeneratorTargetsProvider targetProvider in _targetProviders)
        {
            if (!targetProvider.IsProviderForGenerator(generator)) continue;
            
            provider = targetProvider;
            break;
        }

        return provider;
    }

    private IEnumerable<GenerationCommand> CreateGenerationCommandsForTargets(ISourceGenerator generator, ISourceGeneratorTargetsProvider provider,
        GenerationContext context)
    {
        List<GenerationCommand> commands = new();
        var targets = provider.ClassesRegisteredForGenerator();
        foreach (var target in targets)
        {
            context.CurrentTarget = target;
            if (!generator.ShouldGenerate(context))
            {
                continue;
            }
            commands.Add(CreateSingleGenerationCommand(generator, context));
        }

        return commands;
    }
}