﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ZSourceGenerator;
using ZSourceGenerator.SourceExtension;

namespace SourceExtension
{
    /// <summary>
    /// Source extensions require a targets provider registered to the generator.
    /// The provider is what searches assemblies to find the target types we are going to extend.
    /// </summary>
    public class SourceExtendedTargetsProvider : ISourceGeneratorTargetsProvider
    {
        private readonly Assembly[] _assemblies;
    
        public SourceExtendedTargetsProvider(Assembly[] assemblies)
        {
            _assemblies = assemblies;
        }
    
        public SourceExtendedTargetsProvider() : this(AppDomain.CurrentDomain.GetAssemblies())
        {
        }
        
        public bool IsProviderForGenerator(ISourceGenerator generator)
        {
            return generator is SourceExtensionGenerator;
        }

        public IEnumerable<Type> ClassesRegisteredForGenerator()
        {
            var targetTypes = new List<Type>();

            foreach (var assembly in _assemblies)
            {
                FindTypesOnAssembly(assembly, targetTypes);
            }

            return targetTypes;
        }

        private static bool FilterExtensionAttributeTypes(SourceExtendedAttribute attribute, Type type)
        {
            return typeof(SourceExtensionGenerator).IsAssignableFrom(attribute.ResponsibleExtensionClass);
        }
        
        private static void FindTypesOnAssembly(Assembly assembly, List<Type> targetClasses)
        {
            assembly.GetAllTypesWereAttributeMatch<SourceExtendedAttribute>(FilterExtensionAttributeTypes, targetClasses);
        }
    }
}