﻿using System.Reflection;

namespace SourceExtension
{
    public static class ZGeneratorSourceExtension
    {
        public static void EnableSourceExtensionForAllAssemblies(this ZGenerator sourceGenerator)
        {
            sourceGenerator.RegisterTargetProvider(new SourceExtendedTargetsProvider());
        }
        
        public static void EnableSourceExtensionForAssemblies(this ZGenerator sourceGenerator, params Assembly[] supportedAssemblies)
        {
            sourceGenerator.RegisterTargetProvider(new SourceExtendedTargetsProvider(supportedAssemblies));
        }
    }
}