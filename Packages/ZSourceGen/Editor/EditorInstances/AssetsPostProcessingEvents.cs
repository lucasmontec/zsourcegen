﻿using UI;
using UnityEditor;

namespace EditorInstances
{
    public class AssetsPostProcessingEvents : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            if (ZGeneratorWindow.CurrentWindow is null)
            {
                return;
            }
            
            if (importedAssets.Length > 0 || deletedAssets.Length > 0)
            {
                ZGeneratorWindow.CurrentWindow.RefreshGeneratorAndScan();
            }
        }
    }
}