﻿using GeneratedFilesHandling;

namespace EditorInstances
{
    public static class EditorSourceFilesCache
    {
        private static GeneratedSourceFilesCache editorCache;
        public static GeneratedSourceFilesCache Cache => editorCache ??= CreateAndLoadCache();
        
        private static GeneratedSourceFilesCache CreateAndLoadCache()
        {
            var cache = new GeneratedSourceFilesCache();
            cache.LoadFromFile();
            return cache;
        }

        public static void RemoveFile(string filePath)
        {
            Cache.RemoveFile(filePath);
        }
    }
}