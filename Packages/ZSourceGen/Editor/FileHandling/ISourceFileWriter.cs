﻿namespace FileHandling
{
    public interface ISourceFileWriter
    {
        public void WriteSourceFile(string filePath, string generatedSource);
    }
}