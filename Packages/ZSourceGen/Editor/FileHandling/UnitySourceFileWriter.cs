﻿using System.IO;
using UnityEditor;

namespace FileHandling
{
    public class UnitySourceFileWriter : ISourceFileWriter
    {
        public void WriteSourceFile(string filePath, string generatedSource)
        {
            File.WriteAllText(filePath, generatedSource);
            AssetDatabase.Refresh();
        }
    }
}