﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Settings
{
    public class ZGeneratorSettingsProvider : SettingsProvider
    {
        private const string ZGeneratorSettingsPath = "Project/ZGeneratorSettings";
        private GeneratorSettings _settings;
        private bool _isDirty;

        public static event Action OnSettingsChanged;
        
        private ZGeneratorSettingsProvider(string path, SettingsScope scope = SettingsScope.User) 
            : base(path, scope) 
        {
            _settings = GeneratorSettingsEditorPrefsPersistence.LoadSettings();
        }
        
        public static void OpenGeneratorSettings()
        {
            SettingsService.OpenProjectSettings(ZGeneratorSettingsPath);
        }

        public override void OnGUI(string searchContext)
        {
            EditorGUILayout.LabelField("ZGenerator Settings", EditorStyles.boldLabel);

            EditorGUI.BeginChangeCheck();
            
            _settings.GeneratedFilesHeader = EditorGUILayout.TextField("Generated Files Header", _settings.GeneratedFilesHeader);
            _settings.GeneratedExtension = EditorGUILayout.TextField("Generated Extension", _settings.GeneratedExtension);
            _settings.DefaultGeneratorOutputDirectory = 
                EditorGUILayout.TextField("Default Generator Output Directory", _settings.DefaultGeneratorOutputDirectory);

            _settings.RunAllGeneratorsBeforeBuild = 
                EditorGUILayout.Toggle("Run Generators Before Builds", _settings.RunAllGeneratorsBeforeBuild);
            
            _settings.RunAllGeneratorsBeforeEnteringPlayMode = 
                EditorGUILayout.Toggle("Run Generators Before Play Mode", _settings.RunAllGeneratorsBeforeEnteringPlayMode);
            
            if (EditorGUI.EndChangeCheck())
            {
                _isDirty = true;
            }

            CheckDirtyAndSave();
            
            RenderResetSettingsButton();
        }

        private void RenderResetSettingsButton()
        {
            if (!GUILayout.Button("Reset Settings")) return;
            
            _settings = new GeneratorSettings();
            GeneratorSettingsEditorPrefsPersistence.SaveSettings(_settings);
            Debug.Log("ZGenerator Settings reset to default!");
        }

        private void CheckDirtyAndSave()
        {
            if (!_isDirty) return;
            
            GeneratorSettingsEditorPrefsPersistence.SaveSettings(_settings);
            _isDirty = false;
            OnSettingsChanged?.Invoke();
        }

        [SettingsProvider]
        public static SettingsProvider CreateZGeneratorSettingsProvider()
        {
            var provider = new ZGeneratorSettingsProvider(ZGeneratorSettingsPath, SettingsScope.Project)
            {
                keywords = new HashSet<string>(new[] { "Generator", "ZGenerator" })
            };
            return provider;
        }
    }
}