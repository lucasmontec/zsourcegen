﻿using UnityEditor;
using System;

namespace Settings
{
    public static class GeneratorSettingsEditorPrefsPersistence
    {
        private const string PrefHeaderKey = "ZGeneratorSettings.GeneratedFilesHeader";
        private const string PrefExtensionKey = "ZGeneratorSettings.GeneratedExtension";
        private const string PrefDefaultOutputDirectoryKey = "ZGeneratorSettings.DefaultGeneratorOutputDirectory";
        private const string RunAllGeneratorsBeforeBuildKey = "ZGeneratorSettings.RunAllGeneratorsBeforeBuild";
        private const string RunAllGeneratorsBeforeEnteringPlayModeKey = "ZGeneratorSettings.RunAllGeneratorsBeforeEnteringPlayMode";

        public static GeneratorSettings LoadSettings()
        {
            return new GeneratorSettings
            {
                GeneratedFilesHeader = EditorPrefs.GetString(PrefHeaderKey),
                GeneratedExtension = EditorPrefs.GetString(PrefExtensionKey, ".generated.cs"),
                DefaultGeneratorOutputDirectory = EditorPrefs.GetString(PrefDefaultOutputDirectoryKey),
                RunAllGeneratorsBeforeBuild = EditorPrefs.GetInt(RunAllGeneratorsBeforeBuildKey, 1) != 0,
                RunAllGeneratorsBeforeEnteringPlayMode = EditorPrefs.GetInt(RunAllGeneratorsBeforeEnteringPlayModeKey, 1) != 0
            };
        }

        public static void SaveSettings(GeneratorSettings settings)
        {
            if (settings == null)
                throw new ArgumentNullException(nameof(settings), "Settings cannot be null!");

            EditorPrefs.SetString(PrefHeaderKey, settings.GeneratedFilesHeader);
            EditorPrefs.SetString(PrefExtensionKey, settings.GeneratedExtension);
            EditorPrefs.SetString(PrefDefaultOutputDirectoryKey, settings.DefaultGeneratorOutputDirectory);
            
            EditorPrefs.SetInt(RunAllGeneratorsBeforeBuildKey, settings.RunAllGeneratorsBeforeBuild ? 1 : 0);
            EditorPrefs.SetInt(RunAllGeneratorsBeforeEnteringPlayModeKey, settings.RunAllGeneratorsBeforeEnteringPlayMode ? 1 : 0);
        }
    }
}