﻿using System.IO;

namespace Util
{
    public class DirectoryUtil
    {
        public static void EnsureDirectoryExists(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
    }
}