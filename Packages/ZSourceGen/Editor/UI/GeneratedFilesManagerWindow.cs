﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using EditorInstances;
using UnityEditor;
using UnityEngine;

namespace UI
{
    public class GeneratedFilesManagerWindow : EditorWindow
    {
        private Vector2 _scrollPos;
        private const int MaxPathSizeOnUI = 50;
        private readonly HashSet<string> _filesToDelete = new();

        [MenuItem("Tools/ZSource Generation/Generated Files Manager")]
        public static void ShowWindow()
        {
            GetWindow<GeneratedFilesManagerWindow>("Generated Files Manager");
        }

        private void OnGUI()
        {
            RenderHeader();
            RenderFilesList();
            DeleteScheduledFiles();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void RenderHeader()
        {
            GUILayout.Space(5);
            GUILayout.Label("Managed Files", EditorStyles.boldLabel);
            GUILayout.Space(5);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void RenderFilesList()
        {
            GUILayout.BeginVertical(GUI.skin.box);
            _scrollPos = GUILayout.BeginScrollView(_scrollPos, GUILayout.ExpandHeight(true));
            
            RenderFiles();

            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void RenderFiles()
        {
            HashSet<string> managedFiles = EditorSourceFilesCache.Cache.GetManagedFiles();
            foreach (var filePath in managedFiles)
            {
                GUILayout.BeginHorizontal();
                string displayPath = TruncatePath(filePath, MaxPathSizeOnUI);
                GUIContent labelContent = new(displayPath, filePath);
                GUILayout.Label(labelContent, EditorStyles.label);

                RenderOpenButton(filePath);
                RenderDeleteButton(filePath);

                GUILayout.EndHorizontal();
                GUILayout.Space(5);
            }
        }

        private static string TruncatePath(string path, int maxLength)
        {
            var fileName = Path.GetFileName(path);
            
            if (fileName.Length > maxLength) return fileName;

            if (path.Length <= maxLength) return path;
            
            var truncatedPath = path.Substring(path.Length - maxLength + 3);
            return "..." + truncatedPath;
        }
        
        private static void RenderOpenButton(string filePath)
        {
            if (!GUILayout.Button("Open", GUILayout.MaxWidth(50))) 
                return;
            
            OpenFile(filePath);
        }

        private void RenderDeleteButton(string filePath)
        {
            if (!GUILayout.Button("Delete", GUILayout.MaxWidth(50))) return;
            
            ScheduleDeleteFile(filePath);
        }

        private static void OpenFile(string filePath)
        {
            var scriptAsset = AssetDatabase.LoadAssetAtPath<MonoScript>(filePath);
            if (scriptAsset == null)
            {
                Debug.LogWarning("Failed to find script asset to open. Trying to open with the system default editor...");
                EditorUtility.OpenWithDefaultApp(filePath);
                return;
            }
            AssetDatabase.OpenAsset(scriptAsset);
        }

        private void ScheduleDeleteFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return;
            }
            if (!EditorUtility.DisplayDialog("Confirm Delete",
                    "Are you sure you want to delete this file?",
                    "Delete",
                    "Cancel")) return;

            _filesToDelete.Add(filePath);
        }

        private void DeleteScheduledFiles()
        {
            foreach (string path in _filesToDelete)
            {
                File.Delete(path);
                EditorSourceFilesCache.RemoveFile(path);
                DeleteMetaFile(path);
                AssetDatabase.Refresh();
            }
        }

        private static void DeleteMetaFile(string filePath)
        {
            var meta = filePath + ".meta";
            
            if (!File.Exists(meta))
            {
                return;
            }
            File.Delete(meta);
        }
    }
}