﻿using System.Collections.Generic;
using System.IO;
using Commands;
using Newtonsoft.Json;
using UnityEngine;
using Util;

namespace GeneratedFilesHandling
{
    public class GeneratedSourceFilesCache : IGenerationPostProcessor
    {
        private readonly HashSet<string> _managedGeneratedFiles = new();

        private const string SaveDirectory = "ZSourceGenerator_Data";
        private const string FileName = "generatedFilesCache.json";
        private static readonly string serializedFilePath = Path.Combine(SaveDirectory, FileName);
        
        public HashSet<string> GetManagedFiles()
        {
            return _managedGeneratedFiles;
        }

        public void RemoveFile(string filePath)
        {
            _managedGeneratedFiles.Remove(filePath);
            SaveToFile();
        }
        
        public void Process(List<GenerationCommand> commands)
        {
            foreach (GenerationCommand generationCommand in commands)
            {
                _managedGeneratedFiles.Add(generationCommand.FilePath);
                SaveToFile();
            }
        }

        private void SaveToFile()
        {
            try
            {
                string json = JsonConvert.SerializeObject(_managedGeneratedFiles);

                DirectoryUtil.EnsureDirectoryExists(SaveDirectory);
                File.WriteAllText(serializedFilePath, json);
            }
            catch (System.Exception ex)
            {
                Debug.LogError("Error saving generated source files cache: " + ex.Message);
            }
        }
        
        public void LoadFromFile()
        {
            try
            {
                bool hasSerializedFile = File.Exists(serializedFilePath);
                if (!hasSerializedFile) return;

                string json = File.ReadAllText(serializedFilePath);

                _managedGeneratedFiles.Clear();
                var deserializedSet = JsonConvert.DeserializeObject<HashSet<string>>(json);
                _managedGeneratedFiles.AddRange(deserializedSet);
            }
            catch (System.Exception ex)
            {
                Debug.LogError("Error generated source files cache: " + ex.Message);
            }
        }
    }
}