- - 

## To-Do:

 * Add a remove all generated files on the manager window.
   * Create a method for that on the cache, or a static class for that.
 * After a generation (assembly will probably reload), check if there where any compilation errors. 
   * If there are, show a dialog and offer to delete the generated files.
 * Make an assembly list and source extension setting on the generator settings.
   * Build generator must also use the target assemblies list.