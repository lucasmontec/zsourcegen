﻿using System;

namespace ZSourceGenerator
{
    public struct GenerationContext
    {
        public Type CurrentTarget { get; set; }
    }
}