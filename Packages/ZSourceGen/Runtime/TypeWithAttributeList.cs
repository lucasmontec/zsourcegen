﻿using System;
using System.Collections.Generic;

namespace ZSourceGenerator
{
    public class TypeWithAttributeList<TAttribute> : List<TypeWithAttribute<TAttribute>> 
        where TAttribute : Attribute
    {
    }
}