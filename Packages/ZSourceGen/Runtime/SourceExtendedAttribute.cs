﻿using System;

namespace ZSourceGenerator
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class SourceExtendedAttribute : Attribute
    {
        public Type ResponsibleExtensionClass { get; }

        public SourceExtendedAttribute(Type responsibleExtensionClass)
        {
            ResponsibleExtensionClass = responsibleExtensionClass;
        }
    }
}