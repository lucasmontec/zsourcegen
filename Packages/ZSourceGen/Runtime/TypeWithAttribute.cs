﻿using System;

namespace ZSourceGenerator
{
    public struct TypeWithAttribute<TAttribute> where TAttribute : Attribute
    {
        public Type Type;
        public TAttribute Attribute;
    }
}