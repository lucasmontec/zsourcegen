﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ZSourceGenerator
{
    public static class AssemblyScanExtensions
    {
        public static Assembly[] AllAssemblies => AppDomain.CurrentDomain.GetAssemblies();
        
        public static TypeWithAttributeList<TAttribute> GetAllTypesWithAttribute<TAttribute>
            (this Assembly[] assemblies, TypeWithAttributeList<TAttribute> listToFill=null) 
            where TAttribute : Attribute
        {
            TypeWithAttributeList<TAttribute> result = 
                listToFill ?? new TypeWithAttributeList<TAttribute>();
            
            foreach (Assembly assembly in assemblies)
            {
                assembly.GetAllTypesWithAttribute(result);
            }

            return result;
        }
        
        public static TypeWithAttributeList<TAttribute> GetAllTypesWithAttribute<TAttribute>
            (this Assembly assembly, TypeWithAttributeList<TAttribute> listToFill=null) 
            where TAttribute : Attribute
        {
            TypeWithAttributeList<TAttribute> result = listToFill ?? new TypeWithAttributeList<TAttribute>();
            
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (Type type in assembly.GetTypes())
            {
                var attribute = type.GetCustomAttribute<TAttribute>();
                if (attribute != null)
                {
                    result.Add(new TypeWithAttribute<TAttribute>
                    {
                        Type = type,
                        Attribute = attribute
                    });
                }
            }
            
            return result;
        }
        
        public static List<Type> GetAllTypesWereAttributeMatch<TAttribute>(this Assembly assembly, 
            Func<TAttribute, Type, bool> attributeFilter, List<Type> listToFill=null) 
            where TAttribute : Attribute
        {
            List<Type> result = listToFill ?? new List<Type>();
            
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (Type type in assembly.GetTypes())
            {
                var attribute = type.GetCustomAttribute<TAttribute>();
                if (attribute != null && attributeFilter(attribute, type))
                {
                    result.Add(type);
                }
            }
            
            return result;
        }
    }
}