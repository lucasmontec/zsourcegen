﻿using ZSourceGenerator;
using ZSourceGenerator.CodeGeneration;
using ZSourceGenerator.SourceExtension;

public class TestSourceExtensionGenerator : SourceExtensionGenerator
{
    public override bool ShouldGenerate(GenerationContext context)
    {
        return context.CurrentTarget.Name == "TestExtendableSourceClass"; //Filtering here example
    }

    protected override string ExtendSource(GenerationContext context)
    {
        return CodeGen.AddUsingUnityEngineAndSystem(
            CodeGen.WrapWithTargetNamespace("public partial class TestExtendableSourceClass : MonoBehaviour\n    {\n    }",context.CurrentTarget)
            );
    }
}