using UnityEngine;
using ZSourceGenerator;

namespace SomeComplexNamespace.SubSpace
{
    [SourceExtended(typeof(TestSourceExtensionGenerator))]
    public partial class TestExtendableSourceClass : MonoBehaviour
    {
    }
}
